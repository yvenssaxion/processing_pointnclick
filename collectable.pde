class Collectable {
  
  private String name;
  private PImage gameObjectImage; 
  
  public Collectable(String name, String gameObjectImageFile) {
    this.name = name;
    this.gameObjectImage = loadImage(gameObjectImageFile);
  }
  
  public String getName() {
    return name;
  }
  
  public void draw(int x, int y, int iwidth, int iheight) {
    image(gameObjectImage, x, y, iwidth, iheight);
  }
  
  @Override 
  public boolean equals(Object obj) { 
    if (obj == this) { return true; } 
    if (obj == null || obj.getClass() != this.getClass()) { return false; } 
    Collectable otherCollectable = (Collectable) obj; 
    return otherCollectable.getName().equals(this.name);
  } 

  @Override 
  public int hashCode() { 
    final int prime = 13;
    return prime * this.name.hashCode();
  }
}
