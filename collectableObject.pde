class CollectableObject extends GameObject {
  
  private final InventoryManager inventoryManager;
  private final SceneManager sceneManager;
  
  private Collectable collectable;
  private GameObject replaceWith;
  private boolean willReplaceByAnotherGameObject;
  
  public CollectableObject(String identifier, int x, int y, int owidth, int oheight, Collectable collectable, InventoryManager inventoryManager, SceneManager sceneManager) {
    this(identifier, x, y, owidth, oheight, collectable, null, inventoryManager, sceneManager);
  }
  
  public CollectableObject(String identifier, int x, int y, int owidth, int oheight, Collectable collectable, GameObject replaceWith, InventoryManager inventoryManager, SceneManager sceneManager) {
    super(identifier, x, y, owidth, oheight);
    this.inventoryManager = inventoryManager;
    this.sceneManager = sceneManager;
    
    this.collectable = collectable;
    if(replaceWith != null) {
      this.replaceWith = replaceWith;
      this.willReplaceByAnotherGameObject = true;
    } else {
      this.willReplaceByAnotherGameObject = false;
    }
  }
  
  @Override
  public void draw() {
    collectable.draw(x, y, owidth, oheight);
  }
  
  @Override
  public void mouseClicked() {
    if(mouseIsHovering) {
      inventoryManager.addCollectable(collectable);
      this.sceneManager.getCurrentScene().removeGameObject(this);
      if(willReplaceByAnotherGameObject) {
        this.sceneManager.getCurrentScene().addGameObject(replaceWith);  
      }
    }
  }
}
