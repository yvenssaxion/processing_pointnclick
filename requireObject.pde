class RequireObject extends TextObject {
  
  private final InventoryManager inventoryManager;
  private final SceneManager sceneManager;
  
  private Collectable collectable;
  private GameObject replaceWith;
  private boolean willReplaceByAnotherGameObject;
  
  public RequireObject(String identifier, int x, int y, int owidth, int oheight, String gameObjectImageFile, String text, Collectable collectable, InventoryManager inventoryManager, SceneManager sceneManager) {
    this(identifier, x, y, owidth, oheight, gameObjectImageFile, text, collectable, null, inventoryManager, sceneManager);
  }
  
  public RequireObject(String identifier, int x, int y, int owidth, int oheight, String gameObjectImageFile, String text, Collectable collectable, GameObject replaceWith, InventoryManager inventoryManager, SceneManager sceneManager) {
    super(identifier, x, y, owidth, oheight, gameObjectImageFile, text);
    this.inventoryManager = inventoryManager;
    this.sceneManager = sceneManager;
    
    this.collectable = collectable;
    if(replaceWith != null) {
      this.replaceWith = replaceWith;
      this.willReplaceByAnotherGameObject = true;
    } else {
      this.willReplaceByAnotherGameObject = false;
    }
  }
  
  @Override
  public void mouseClicked() {
    if(inventoryManager.containsCollectable(collectable) && mouseIsHovering) {
      inventoryManager.removeCollectable(collectable);
      this.sceneManager.getCurrentScene().removeGameObject(this);
      if(willReplaceByAnotherGameObject) {
        this.sceneManager.getCurrentScene().addGameObject(replaceWith);
      }
    } else {
      super.mouseClicked();
    }
  }
  
}
