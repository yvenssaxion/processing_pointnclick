class MoveToSceneObject extends GameObject {
  
  private final SceneManager sceneManager;
  private String nextSceneIdentifier;
  private boolean moveBack;
  
  public MoveToSceneObject(String identifier, int x, int y, int owidth, int oheight, boolean moveBack, SceneManager sceneManager) {
    this(identifier, x, y, owidth, oheight, "", moveBack, sceneManager);
  }
  
  public MoveToSceneObject(String identifier, int x, int y, int owidth, int oheight, String gameObjectImageFile, boolean moveBack, SceneManager sceneManager) {
    super(identifier, x, y, owidth, oheight, gameObjectImageFile);
    this.moveBack = moveBack;
    this.sceneManager = sceneManager;
  }
  
  public MoveToSceneObject(String identifier, int x, int y, int owidth, int oheight, String nextSceneIdentifier, SceneManager sceneManager) {
    this(identifier, x, y, owidth, oheight, "", nextSceneIdentifier, sceneManager);
  }
  
  public MoveToSceneObject(String identifier, int x, int y, int owidth, int oheight, String gameObjectImageFile, String nextSceneIdentifier, SceneManager sceneManager) {
    super(identifier, x, y, owidth, oheight, gameObjectImageFile);
    this.sceneManager = sceneManager;
    this.nextSceneIdentifier = nextSceneIdentifier;
    this.moveBack = false;
  }
  
  @Override
  public void mouseClicked() {
    if(mouseIsHovering) {
      if(moveBack) {
        sceneManager.goToPreviousScene();
      } else {
        try {
          sceneManager.goToScene(nextSceneIdentifier);
        } catch(Exception e) { 
          println(e.getMessage());
        }
      }
    }
  }
}
