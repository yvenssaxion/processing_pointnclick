import java.util.Stack;
import java.util.HashMap;

class SceneManager {
  
  private HashMap<String, Scene> scenes;
  private Stack<Scene> scenesStack;
  
  public SceneManager() {
    scenes = new HashMap<String, Scene>();
    scenesStack = new Stack<Scene>();
  }

  /**
    Adds a new Scene to the scenes HashMap, but doesn't change the scenesStack if the stack is not empty.
    If the stack is empty, then it will place the current scene as the first one.
  */
  public void addScene(Scene scene) {
    scenes.put(scene.getSceneName(), scene);
    if(scenesStack.size() == 0)
    {
      scenesStack.push(scene);
    }
  }
  
  public void goToScene(String sceneName) throws Exception {
    if(scenes.containsKey(sceneName)) {
      scenesStack.push(scenes.get(sceneName));
    }
    else {
      throw new Exception("Scene not found with name: "+ sceneName + ". Make sure it was added to the sceneManager.");
    }
  }
  
  public void goToPreviousScene() {
    scenesStack.pop();
  }
  
  /**
    Returns the scene in the top of the scenesStack.
  */
  public Scene getCurrentScene() {
    return scenesStack.peek();
  }
}
